package service

const (
	// FoodCode represent the taxcode for Food
	FoodCode = iota + 1
	// TobaccoCode represent the taxcode for Tobbacco
	TobaccoCode
	// EntertainmentCode representthe taxcode for Entertainment
	EntertainmentCode
)

var (
	// Taxes represent as the mapping taxcode
	Taxes = map[int]TaxCode{
		FoodCode: TaxCode{
			Type:       "Food & Beverage",
			Refundable: true,
			CountTax: func(price float64) (res float64) {
				res = price * 0.1
				return
			},
		},
		TobaccoCode: TaxCode{
			Type: "Tobacco",
			CountTax: func(price float64) (res float64) {
				res = 10 + (price * 0.02)
				return
			},
		},
		EntertainmentCode: TaxCode{
			Type: "Entertainment",
			CountTax: func(price float64) (res float64) {
				if price < 100 {
					return
				}
				res = (0.01 * (price - 100))
				return
			},
		},
	}
)

// TaxCode represent the tax-code data struct
type TaxCode struct {
	Type       string
	Refundable bool
	CountTax   TaxCounter
}

// TaxCounter ...
type TaxCounter func(price float64) float64
