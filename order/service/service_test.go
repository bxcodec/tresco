package service_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"

	"bitbucket.org/bxcodec/tresco/domain"
	"bitbucket.org/bxcodec/tresco/domain/mocks"
	"bitbucket.org/bxcodec/tresco/order/service"
)

func TestInsert(t *testing.T) {
	repo := new(mocks.OrderRepository)
	srv := service.New(repo, time.Second*2)
	mockOrder := domain.Order{
		Name:    "Big Mac",
		TaxCode: service.FoodCode,
		Price:   1000,
	}
	t.Run("success", func(t *testing.T) {
		repo.On("Insert", mock.Anything, &mockOrder).Return(func() error {
			mockOrder.ID = "11"
			return nil
		}()).Once()

		err := srv.Insert(context.Background(), &mockOrder)
		require.NoError(t, err)
		require.Equal(t, "11", mockOrder.ID)
		repo.AssertExpectations(t)
	})

	t.Run("error from repository", func(t *testing.T) {
		repo.On("Insert", mock.Anything, &mockOrder).Return(errors.New("Unknown Error")).Once()

		err := srv.Insert(context.Background(), &mockOrder)
		require.Error(t, err)
		repo.AssertExpectations(t)
	})
}

func TestGetTaxes(t *testing.T) {
	repo := new(mocks.OrderRepository)
	srv := service.New(repo, time.Second*2)
	mockOrders := []domain.Order{
		domain.Order{
			ID:      "12",
			Name:    "Lucky Stretch",
			TaxCode: service.TobaccoCode,
			Price:   1000,
		},
		domain.Order{
			ID:      "13",
			Name:    "Big Mac",
			TaxCode: service.FoodCode,
			Price:   1000,
		},
		domain.Order{
			ID:      "12",
			Name:    "Movie",
			TaxCode: service.EntertainmentCode,
			Price:   150,
		},
	}

	t.Run("success", func(t *testing.T) {
		repo.On("Get", mock.Anything).Return(mockOrders, nil).Once()

		res, err := srv.Get(context.Background())
		require.NoError(t, err)
		require.Len(t, res, len(mockOrders))

		expectedResult := []domain.Order{
			domain.Order{
				ID:         "12",
				Name:       "Lucky Stretch",
				TaxCode:    service.TobaccoCode,
				Price:      1000,
				Type:       "Tobacco",
				Refundable: false,
				Tax:        30,
				Amount:     1030,
			},
			domain.Order{
				ID:         "13",
				Name:       "Big Mac",
				TaxCode:    service.FoodCode,
				Price:      1000,
				Type:       "Food & Beverage",
				Refundable: true,
				Tax:        100,
				Amount:     1100,
			},
			domain.Order{
				ID:         "12",
				Name:       "Movie",
				TaxCode:    service.EntertainmentCode,
				Price:      150,
				Type:       "Entertainment",
				Refundable: false,
				Tax:        0.5,
				Amount:     150.5,
			},
		}

		require.Equal(t, expectedResult, res)
		repo.AssertExpectations(t)
	})

	t.Run("failed from repository", func(t *testing.T) {
		repo.On("Get", mock.Anything).Return([]domain.Order{}, errors.New("Unknown Error")).Once()

		res, err := srv.Get(context.Background())
		require.Error(t, err)
		require.Len(t, res, 0)
		repo.AssertExpectations(t)
	})
}
