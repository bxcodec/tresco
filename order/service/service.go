package service

import (
	"context"
	"time"

	"bitbucket.org/bxcodec/tresco/domain"
)

type service struct {
	repo    domain.OrderRepository
	timeout time.Duration
}

// New will create an implementation of domain.OrderService
func New(repo domain.OrderRepository, timeout time.Duration) domain.OrderService {
	return &service{
		repo:    repo,
		timeout: timeout,
	}
}

func validate(o *domain.Order) (err error) {
	_, ok := Taxes[o.TaxCode]
	if !ok {
		return domain.ErrBadRequest
	}

	if o.Price == 0 {
		return domain.ErrBadRequest
	}

	if o.Name == "" {
		return domain.ErrBadRequest
	}
	return
}

func (s service) Insert(ctx context.Context, o *domain.Order) (err error) {
	if ctx == nil {
		return domain.ErrContextNil
	}
	ctx, cancel := context.WithTimeout(ctx, s.timeout)
	defer cancel()
	err = validate(o)
	if err != nil {
		return
	}
	o.CreatedTime = time.Now()
	err = s.repo.Insert(ctx, o)
	if err != nil {
		return
	}
	o.Type = Taxes[o.TaxCode].Type
	o.Refundable = Taxes[o.TaxCode].Refundable
	o.Tax = Taxes[o.TaxCode].CountTax(o.Price)
	o.Amount = o.Tax + o.Price
	return
}

func (s service) Get(ctx context.Context) (res []domain.Order, err error) {
	if ctx == nil {
		err = domain.ErrContextNil
		return
	}
	ctx, cancel := context.WithTimeout(ctx, s.timeout)
	defer cancel()

	res, err = s.repo.Get(ctx)
	if err != nil {
		return
	}
	finalRes := []domain.Order{}
	for _, order := range res {
		if order.TaxCode == 0 {
			continue
		}
		taxes := Taxes[order.TaxCode]
		if taxes.Type == "" {
			return
		}
		order.Type = Taxes[order.TaxCode].Type
		order.Refundable = Taxes[order.TaxCode].Refundable
		order.Tax = Taxes[order.TaxCode].CountTax(order.Price)
		order.Amount = order.Tax + order.Price
		finalRes = append(finalRes, order)
	}
	res = finalRes
	return
}
