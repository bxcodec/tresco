package mysql_test

import (
	"context"
	"log"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"

	dbSuite "bitbucket.org/bxcodec/tresco/database/mysql"
	"bitbucket.org/bxcodec/tresco/domain"
	"bitbucket.org/bxcodec/tresco/order/repository/mysql"
)

type OrderSuite struct {
	dbSuite.SuiteTest
}

func TestOrderSuite(t *testing.T) {
	if testing.Short() {
		t.Skip("Skip order mysql repository test")
	}
	dsn := os.Getenv("MYSQL_TEST_URL")
	if dsn == "" {
		dsn = "root:root-pass@tcp(localhost:33060)/tresco?parseTime=1&loc=Asia%2FJakarta&charset=utf8mb4&collation=utf8mb4_unicode_ci"
	}
	orderSuite := &OrderSuite{
		SuiteTest: dbSuite.NewSuite(dsn),
	}

	suite.Run(t, orderSuite)
}

func (o *OrderSuite) SetupTest() {
	log.Println("Starting a Test. Migrating the Database")
	_, err := o.Migration.Up()
	require.NoError(o.T(), err)
	log.Println("Database Migrated Successfully")
}

func (o *OrderSuite) TearDownTest() {
	log.Println("Finishing Test. Dropping The Database")
	_, err := o.Migration.Down()
	require.NoError(o.T(), err)
	log.Println("Database Dropped Successfully")
}

func (o *OrderSuite) TestInsert() {
	mockOrder := &domain.Order{
		Name:        "Big Mac",
		TaxCode:     1,
		Price:       1000,
		CreatedTime: time.Now(),
	}
	require.Empty(o.T(), mockOrder.ID)
	repo := mysql.NewRepository(o.DBConn)

	err := repo.Insert(context.Background(), mockOrder)
	require.NoError(o.T(), err)
	require.NotEmpty(o.T(), mockOrder.ID)
	// Ensure the item is inserted
	query := "SELECT id, name, tax_code, price FROM `order` WHERE id=?"
	row := o.DBConn.QueryRowContext(context.Background(), query, mockOrder.ID)
	var res domain.Order
	err = row.Scan(
		&res.ID,
		&res.Name,
		&res.TaxCode,
		&res.Price,
	)
	require.NoError(o.T(), err)

	require.Equal(o.T(), mockOrder.Name, res.Name)
	require.Equal(o.T(), mockOrder.TaxCode, res.TaxCode)
	require.Equal(o.T(), mockOrder.Price, res.Price)
}

func (o *OrderSuite) seed() {
	mockOrders := []domain.Order{
		domain.Order{
			Name:        "Lucky Stretch",
			TaxCode:     2,
			Price:       1000,
			CreatedTime: time.Now(),
		},
		domain.Order{
			Name:        "Big Mac",
			TaxCode:     1,
			Price:       1000,
			CreatedTime: time.Now(),
		},
		domain.Order{
			Name:        "Movie",
			TaxCode:     3,
			Price:       150,
			CreatedTime: time.Now(),
		},
	}

	query := "INSERT `order` SET name=?, tax_code=?, price=?, created_time=?"
	for _, item := range mockOrders {
		res, err := o.DBConn.Exec(query, item.Name, item.TaxCode, item.Price, item.CreatedTime)
		require.NoError(o.T(), err)
		_, err = res.LastInsertId()
		require.NoError(o.T(), err)
	}
}

func (o *OrderSuite) TestGet() {
	o.seed()

	repo := mysql.NewRepository(o.DBConn)
	res, err := repo.Get(context.Background())
	require.NoError(o.T(), err)
	require.Len(o.T(), res, 3)

	require.Equal(o.T(), "Lucky Stretch", res[0].Name)
	require.Equal(o.T(), 2, res[0].TaxCode)
	require.Equal(o.T(), float64(1000), res[0].Price)

}
