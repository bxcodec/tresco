package mysql

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/sirupsen/logrus"

	"bitbucket.org/bxcodec/tresco/domain"
)

type repository struct {
	DB *sql.DB
}

// NewRepository will return the iplementation of mysql order repository
func NewRepository(db *sql.DB) domain.OrderRepository {
	return &repository{
		DB: db,
	}
}

func (r repository) Insert(ctx context.Context, o *domain.Order) (err error) {
	// query := `INSERT INTO order(name, tax_code, price, created_time) VALUES(?,?,?,?)`
	query := "INSERT `order` SET name=?, tax_code=?, price=?, created_time=?"

	res, err := r.DB.ExecContext(ctx, query, o.Name, o.TaxCode, o.Price, o.CreatedTime)
	if err != nil {
		return
	}

	id, err := res.LastInsertId()
	if err != nil {
		return
	}
	o.ID = fmt.Sprintf("%d", id)
	return
}

func (r repository) Get(ctx context.Context) (res []domain.Order, err error) {
	query := "SELECT id, name, tax_code, price FROM `order` ORDER BY id"

	rows, err := r.DB.QueryContext(ctx, query)
	if err != nil {
		return
	}
	defer func() {
		err := rows.Close()
		if err != nil {
			logrus.Error(err)
		}
	}()

	for rows.Next() {
		var o domain.Order
		err = rows.Scan(
			&o.ID,
			&o.Name,
			&o.TaxCode,
			&o.Price,
		)
		if err != nil {
			return
		}

		res = append(res, o)
	}

	return
}
