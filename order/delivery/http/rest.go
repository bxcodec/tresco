package http

import (
	"net/http"

	"github.com/labstack/echo"
	"github.com/sirupsen/logrus"

	"bitbucket.org/bxcodec/tresco/domain"
)

type orderHandler struct {
	service domain.OrderService
}

// InitOrderHandler will initialize the order HTTP handler
func InitOrderHandler(e *echo.Echo, service domain.OrderService) {
	handler := &orderHandler{
		service: service,
	}
	e.GET("/order", handler.Get)
	e.POST("/order", handler.Insert)
}

func (o orderHandler) Insert(c echo.Context) (err error) {
	var item domain.Order
	err = c.Bind(&item)
	if err != nil {
		logrus.Error(err)
		return c.JSON(http.StatusBadRequest, "Bad Input Param")
	}
	ctx := c.Request().Context()

	err = o.service.Insert(ctx, &item)
	if err != nil {
		logrus.Error(err)
		if err == domain.ErrBadRequest {
			return c.JSON(http.StatusBadRequest, "Bad Input Param")
		}
		return c.JSON(http.StatusInternalServerError, "Internal Server Error")
	}

	return c.JSON(http.StatusCreated, item)
}

func (o orderHandler) Get(c echo.Context) (err error) {
	ctx := c.Request().Context()

	res, err := o.service.Get(ctx)
	if err != nil {
		logrus.Error(err)
		return c.JSON(http.StatusInternalServerError, "Internal Server Error")
	}

	return c.JSON(http.StatusOK, res)
}
