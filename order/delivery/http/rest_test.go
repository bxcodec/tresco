package http_test

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/labstack/echo"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"

	"bitbucket.org/bxcodec/tresco/domain"
	"bitbucket.org/bxcodec/tresco/domain/mocks"
	handler "bitbucket.org/bxcodec/tresco/order/delivery/http"
	"bitbucket.org/bxcodec/tresco/order/service"
)

func TestInsert(t *testing.T) {
	e := echo.New()
	mockRequestBody := `{
		"name": "Big Mac",
		"taxCode": 1,
		"price": 1000
	  }`
	mockExpectedInsertedOrderItem := &domain.Order{
		Name:    "Big Mac",
		TaxCode: 1,
		Price:   1000,
	}
	t.Run("success", func(t *testing.T) {
		req := httptest.NewRequest("POST", "/order", strings.NewReader(mockRequestBody))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSON)

		rec := httptest.NewRecorder()

		mockService := new(mocks.OrderService)
		mockService.On("Insert", mock.Anything, mockExpectedInsertedOrderItem).Return(nil).Once()

		handler.InitOrderHandler(e, mockService)

		e.ServeHTTP(rec, req)
		require.Equal(t, http.StatusCreated, rec.Code)
		mockService.AssertExpectations(t)
	})

	t.Run("bad input param", func(t *testing.T) {
		req := httptest.NewRequest("POST", "/order", strings.NewReader(`{"wrong json Format",}`))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSON)

		rec := httptest.NewRecorder()

		mockService := new(mocks.OrderService)

		handler.InitOrderHandler(e, mockService)

		e.ServeHTTP(rec, req)
		require.Equal(t, http.StatusBadRequest, rec.Code)
		mockService.AssertExpectations(t)
	})

	t.Run("internal server error", func(t *testing.T) {
		req := httptest.NewRequest("POST", "/order", strings.NewReader(mockRequestBody))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSON)

		rec := httptest.NewRecorder()

		mockService := new(mocks.OrderService)
		mockService.On("Insert", mock.Anything, mockExpectedInsertedOrderItem).Return(errors.New("Internal Server Error")).Once()

		handler.InitOrderHandler(e, mockService)

		e.ServeHTTP(rec, req)
		require.Equal(t, http.StatusInternalServerError, rec.Code)
		mockService.AssertExpectations(t)
	})

	t.Run("bad input param from service", func(t *testing.T) {
		req := httptest.NewRequest("POST", "/order", strings.NewReader(mockRequestBody))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSON)

		rec := httptest.NewRecorder()

		mockService := new(mocks.OrderService)
		mockService.On("Insert", mock.Anything, mockExpectedInsertedOrderItem).Return(domain.ErrBadRequest).Once()

		handler.InitOrderHandler(e, mockService)

		e.ServeHTTP(rec, req)
		require.Equal(t, http.StatusBadRequest, rec.Code)
		mockService.AssertExpectations(t)
	})
}

func TestGetOrder(t *testing.T) {
	e := echo.New()
	mockOrders := []domain.Order{
		domain.Order{
			ID:      "12",
			Name:    "Lucky Stretch",
			TaxCode: service.TobaccoCode,
			Price:   1000,
		},
		domain.Order{
			ID:      "13",
			Name:    "Big Mac",
			TaxCode: service.FoodCode,
			Price:   1000,
		},
		domain.Order{
			ID:      "12",
			Name:    "Movie",
			TaxCode: service.EntertainmentCode,
			Price:   150,
		},
	}

	t.Run("success", func(t *testing.T) {
		req := httptest.NewRequest("GET", "/order", nil)
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSON)

		rec := httptest.NewRecorder()

		mockService := new(mocks.OrderService)
		mockService.On("Get", mock.Anything).Return(mockOrders, nil).Once()

		handler.InitOrderHandler(e, mockService)

		e.ServeHTTP(rec, req)
		require.Equal(t, http.StatusOK, rec.Code)
		mockService.AssertExpectations(t)
	})

	t.Run("internal server error", func(t *testing.T) {
		req := httptest.NewRequest("GET", "/order", nil)
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSON)

		rec := httptest.NewRecorder()

		mockService := new(mocks.OrderService)
		mockService.On("Get", mock.Anything).Return(nil, errors.New("Internl Server Error")).Once()

		handler.InitOrderHandler(e, mockService)

		e.ServeHTTP(rec, req)
		require.Equal(t, http.StatusInternalServerError, rec.Code)
		mockService.AssertExpectations(t)
	})
}
