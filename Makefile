APP_NAME=tresco 
MYSQL_USER=tresco
MYSQL_PASSWORD=tresco-pass
MYSQL_ADDRESS=localhost:3306
MYSQL_DATABASE=tresco

tresco:
	@go build -o $(APP_NAME) app/main.go

lint-prepare:
	@echo "Preparing Linter"
	curl -sfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh| sh -s latest
lint:
	./bin/golangci-lint run \
		--exclude-use-default=false \
		--enable=golint \
		--enable=gocyclo \
		--enable=goconst \
		--enable=unconvert \
		./...

mockery-prepare:
	@echo "Installing mockery"
	@go get -u github.com/vektra/mockery/...

test: mysql-test-up
	@go test -v -race ./...
	@make mysql-test-down

.PHONY: mysql-test-up
mysql-test-up:
	@docker-compose -f docker-compose-test.yaml up -d 

.PHONY: mysql-test-down
mysql-test-down:
	@docker-compose -f docker-compose-test.yaml down

unittest:
	@go test -short -v -race ./...

docker:
	@docker build -t $(APP_NAME) .

run:
	@docker-compose -f docker-compose.yaml up  -d 

stop:
	@docker-compose down

.PHONY: lint-prepare lint mockery-prepare test unittest docker run stop

.PHONY: migrate-prepare
migrate-prepare:
	@go get -u -d github.com/golang-migrate/migrate
	@go build -tags "mysql" -o ./bin/migrate github.com/golang-migrate/migrate/cli

.PHONY: migrate-up
migrate-up:
	@./bin/migrate -database "mysql://$(MYSQL_USER):$(MYSQL_PASSWORD)@tcp($(MYSQL_ADDRESS))/$(MYSQL_DATABASE)" \
	-path=database/mysql/migrations up

.PHONY: migrate-down
migrate-down:
	@./bin/migrate -database "mysql://$(MYSQL_USER):$(MYSQL_PASSWORD)@tcp($(MYSQL_ADDRESS))/$(MYSQL_DATABASE)" \
	-path=database/mysql/migrations down	
