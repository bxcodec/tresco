# tresco

Tresco (Cornish: Enys Skaw, meaning "island of elder-trees") is the second-biggest island of the Isles of Scilly in Cornwall, England.

But, this Tresco here is different with the Tresco island, this one is a Tresco for counting tax. Tresco (**T**ax **Res**ult **Co**unter) is built from Golang as the main programming language.
And as for the architecture, I use my own invented architecture here at https://github.com/bxcodec/go-clean-arch.

In that repo, there is a PR that still on review, I will appreciate if anyone who read this also take a look and give some advice to discuss about the proposed new architecture in that PR as part of my learning journey in software architecture 🙏.


## Documentations and Endpoints

Please refer to the [docs/openapi.yaml](/docs/openapi.yaml) with [Swagger Editor](https://editor.swagger.io/).

## Development

##### Prerequisite
 - Go 1.12 (support go module)
 - Unix-family Workspace (Linux or MacOS) *the application developed under MacOS.

##### Checkout

First, clone the application to any directory as long as not in GOPATH (since this project use Go Module, so clonning in GOPATH is not recommended)

```bash
$ git clone git@bitbucket.org:bxcodec/tresco.git your_target_directory

$ cd your_target_directory
```

## Preparing workspace

##### Install Linter

```bash
$ make lint-prepare
```

##### Apply Linter

```bash
$ make lint
```
 
## Testing
For the testing, there are 2 kind available test included in this projects.

##### Unit Testing

```bash
$ make unittest
```

##### Integration Testing

```bash
$ make test
```

## Running the Application

Follow this steps to run the applications locally.

##### Install Migrations Tools

```bash
$ make migrate-prepare
```

##### Dockerize

```bash
$ make docker
```

##### Prepare the config file
Please create a `config.toml` file in the root project. The configuration example can be looked in [config.toml.example](/config.toml.example).
Or you can just copy this to run locally.

```toml
debug=true
server=":9090"
contextTimeout=2 #in second
[mysql]
    host="mysql"
    port="3306"
    user="tresco"
    pass="tresco-pass"
    name="tresco"
    lifetime=10 #In Minutes
    max_idle_conns=25
    max_open_conns=100
```

##### Run the services

```
$ docker-compose up -d
#OR
$ make run

# Ensure the services live. There should be 2 container run, `tresco_api`, `tresco_mysql`.
$ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                               NAMES
612f998b0aa1        tresco              "/bin/sh -c /app/tre…"   23 seconds ago      Up 5 seconds        0.0.0.0:9090->9090/tcp              tresco_api
de448de3574c        mysql:5.7           "docker-entrypoint.s…"   24 seconds ago      Up 23 seconds       0.0.0.0:3306->3306/tcp, 33060/tcp   tresco_mysql
```

Since the Mysql will takes time longer to ready to used, please ensure the service is deployed successfully, if not, try again until it live.

##### Run the migration

```bash
$ make migrate-up
```

After run the migrations, the applications should be ready to use at `http://localhost:9090/order`
```
$ curl -v http://localhost:9090/order
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 9090 (#0)
> GET /order HTTP/1.1
> Host: localhost:9090
> User-Agent: curl/7.54.0
> Accept: */*
>
< HTTP/1.1 200 OK
< Content-Type: application/json; charset=UTF-8
< Date: Mon, 06 May 2019 04:22:53 GMT
< Content-Length: 3
<
[]
* Connection #0 to host localhost left intact
```