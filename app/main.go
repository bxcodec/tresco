package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"time"

	delivery "bitbucket.org/bxcodec/tresco/order/delivery/http"
	orderRepo "bitbucket.org/bxcodec/tresco/order/repository/mysql"
	orderService "bitbucket.org/bxcodec/tresco/order/service"
	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

func init() {
	initConfig()
}

func initConfig() {
	viper.SetConfigType("toml")
	viper.SetConfigFile("config.toml")
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal(err)
	}

	logrus.Info("Using Config file: ", viper.ConfigFileUsed())

	if viper.GetBool("debug") {
		logrus.SetLevel(logrus.DebugLevel)
		logrus.Warn("Rome is Running in Debug Mode")
		return
	}
	logrus.SetLevel(logrus.InfoLevel)
	logrus.Warn("Rome is Running in Production Mode")
}

func main() {
	dbHost := viper.GetString(`mysql.host`)
	dbPort := viper.GetString(`mysql.port`)
	dbUser := viper.GetString(`mysql.user`)
	dbPass := viper.GetString(`mysql.pass`)
	dbName := viper.GetString(`mysql.name`)
	dsn := dbUser + `:` + dbPass + `@tcp(` + dbHost + `:` + dbPort + `)/` + dbName + `?parseTime=1&loc=Asia%2FJakarta`
	logrus.Info(`Connecting to database`)
	logrus.Info("Connection DSN: ", dsn)
	dbConn, err := sql.Open(`mysql`, dsn)
	if err != nil {
		logrus.Error(fmt.Sprintf(`Database connection failed. Err: %v`, err.Error()))
		os.Exit(1)
	}
	// SET Connection Pooling to Database by Type
	dbLifetime := viper.GetInt("mysql.lifetime")
	maxIddleCon := viper.GetInt("mysql.max_idle_conns")
	maxOpenCon := viper.GetInt("mysql.max_open_conns")
	dbConn.SetConnMaxLifetime(time.Duration(dbLifetime) * time.Minute)
	dbConn.SetMaxIdleConns(maxIddleCon)
	dbConn.SetMaxOpenConns(maxOpenCon)

	err = dbConn.Ping()
	if err != nil {
		logrus.Error(fmt.Sprintf(`Database connection failed. Err: %v`, err.Error()))
		os.Exit(1)
	}

	contextTimeout := time.Duration(viper.GetInt("contextTimeout")) * time.Second

	e := echo.New()

	repo := orderRepo.NewRepository(dbConn)
	serv := orderService.New(repo, contextTimeout)
	delivery.InitOrderHandler(e, serv)

	logrus.Fatal(e.Start(viper.GetString("server")))
}
