package domain

import (
	"context"
	"time"
)

// Order represent the order data struct model
type Order struct {
	ID          string    `json:"id"`
	Name        string    `json:"name"`
	TaxCode     int       `json:"taxCode"`
	Price       float64   `json:"price"`
	Type        string    `json:"type"`
	Refundable  bool      `json:"refundable"`
	Tax         float64   `json:"tax"`
	Amount      float64   `json:"amount"`
	CreatedTime time.Time `json:"-"`
}

// OrderService is the contract service that available for order operations
type OrderService interface {
	Insert(ctx context.Context, o *Order) (err error)
	Get(ctx context.Context) (res []Order, err error)
}

// OrderRepository is the contract for repository
type OrderRepository interface {
	Insert(ctx context.Context, o *Order) (err error)
	Get(ctx context.Context) (res []Order, err error)
}
