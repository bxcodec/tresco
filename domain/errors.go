package domain

import "errors"

var (
	// ErrContextNil will throw if any context that passed to a function is a nil context
	ErrContextNil = errors.New("Context is Nil")
	// ErrBadRequest will throw if any request body or param is invalid
	ErrBadRequest = errors.New("Requested body/param is not valid")
)
