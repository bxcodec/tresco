package mysql

import (
	"database/sql"
	"log"
	"path"
	"runtime"
	"strings"

	driverSql "github.com/go-sql-driver/mysql"
	"github.com/golang-migrate/migrate"
	_mysql "github.com/golang-migrate/migrate/database/mysql"
	_ "github.com/golang-migrate/migrate/source/file" // for migration helper
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
)

// SuiteTest struct for MySQL Suite
type SuiteTest struct {
	suite.Suite
	DSN       string
	DBConn    *sql.DB
	Migration *migration
	DBName    string
}

// NewSuite return a MysqlSuite test
func NewSuite(dsn string) SuiteTest {
	return SuiteTest{
		DSN: dsn,
	}
}

// SetupSuite setup at the beginning of test
func (s *SuiteTest) SetupSuite() {
	DisableLogging()

	var err error

	s.DBConn, err = sql.Open("mysql", s.DSN)
	require.NoError(s.T(), err)

	// Wait until the database is live
	for {
		err = s.DBConn.Ping()
		if err == nil {
			break
		}
	}

	_, err = s.DBConn.Exec("set global sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';")
	require.NoError(s.T(), err)
	_, err = s.DBConn.Exec("set session sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';")
	require.NoError(s.T(), err)

	_, filename, _, ok := runtime.Caller(0)
	require.True(s.T(), ok)
	migrationPath := path.Join(path.Dir(filename), "migrations")

	s.Migration, err = runMigration(s.DBConn, migrationPath)
	require.NoError(s.T(), err)
}

// TearDownSuite teardown at the end of test
func (s *SuiteTest) TearDownSuite() {
	err := s.DBConn.Close()
	require.NoError(s.T(), err)
}

// DisableLogging ...
func DisableLogging() {
	nopLogger := NopLogger{}
	err := driverSql.SetLogger(nopLogger)
	if err != nil {
		log.Fatal(err)
	}
}

// NopLogger no operation logger
type NopLogger struct {
}

// Print ...
func (l NopLogger) Print(v ...interface{}) {}

// Migration helper for testing

type migration struct {
	Migrate *migrate.Migrate
}

func (m *migration) Up() (bool, error) {
	err := m.Migrate.Up()
	if err != nil {
		if err == migrate.ErrNoChange {
			return true, err
		}
		return false, nil
	}
	return true, nil
}

func (m *migration) Down() (bool, error) {
	err := m.Migrate.Down()
	if err != nil {
		return false, err
	}
	return true, nil
}

func runMigration(dbConn *sql.DB, migrationsFolderLocation string) (*migration, error) {
	dataPath := []string{}
	dataPath = append(dataPath, "file://")
	dataPath = append(dataPath, migrationsFolderLocation)

	pathToMigrate := strings.Join(dataPath, "")

	driver, err := _mysql.WithInstance(dbConn, &_mysql.Config{})
	if err != nil {
		return nil, err
	}

	m, err := migrate.NewWithDatabaseInstance(pathToMigrate, "mysql", driver)
	if err != nil {
		return nil, err
	}
	return &migration{Migrate: m}, nil
}
