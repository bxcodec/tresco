## Builder
FROM golang:1.12-alpine3.9 as builder

RUN apk update && apk upgrade && \
    apk --no-cache --update add git make && \
    go get -u github.com/golang/dep/cmd/dep

WORKDIR /go_module/tresco

COPY . .

RUN go mod download

RUN make tresco

## Distribution
FROM alpine:latest

RUN apk update && apk upgrade && \
    apk --no-cache --update add ca-certificates tzdata && \
    mkdir /app && mkdir tresco

WORKDIR /tresco

EXPOSE 9090

COPY --from=builder /go_module/tresco/tresco /app

CMD /app/tresco

 